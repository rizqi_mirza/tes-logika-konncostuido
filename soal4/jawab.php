<?php
    function bubble_sort($bilangan) {
        $size = count($bilangan)-1;
        for ($i=0; $i<$size; $i++) {
            for ($j=0; $j<$size-$i; $j++) {
                $k = $j+1;
                if ($bilangan[$k] < $bilangan[$j]) {
                    // Swap elements at indices: $j, $k
                    list($bilangan[$j], $bilangan[$k]) = array($bilangan[$k], $bilangan[$j]);
                }
            }
        }
        return $bilangan;
    }

    $bilangan = array(99, 2, 64, 8, 111, 33, 65, 11, 102, 50);

    print("Sebelum menggunakan Bubble Sorting");
    print_r($bilangan);
    echo "<br>";
    $bilangan = bubble_sort($bilangan);
    print("Sesudah menggunakan Bubble Sorting");
    print_r($bilangan);
?>