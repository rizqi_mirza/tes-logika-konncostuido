<?php
 
//function untuk pengecekan bilangan prima
function cekprima($nilai)
{
   $dibagi = 0;
 
   // membuat pembagian dengan looping
   // jika hasil bagi 0 atau habis dibagi maka $dibagi bertambah
   for ($i = 1; $i <= $nilai; $i++) {
      if ($nilai % $i == 0) {
         $dibagi++;
      }
   }
 
   // oleh angka dibawahnya
   if ($dibagi == 2) {
      echo $nilai . "\x20adalah bilangan prima\n";
   } else {
      echo $nilai . "\x20bukan bilangan prima\n";
   }
}

//function pengecekan nilai
cekprima(61);

?>